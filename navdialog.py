#!/usr/bin/env python
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Aasish Pappu (aasish@cs.cmu.edu)                             ##
##  Date  : November 2011                                                ##
###########################################################################
#Description: DialogManager for StreetView
###########################################################################

#from teamtalk import geoDBase,navigateUtilObj
from navdb import GeoNames
from navutils import NavUtil
#global geoDBase,navigateUtilObj


streetViewCommands = ["start driving",
                      "drive", "stop driving",
                      "fast", "slow", "normal", "faster"]

turnCommands = ["turn right", "turn left", "turn around", "right", "left", "around"]
speedMap = {"slow": "3", "normal": "2", "fast": "1", "faster": "0"}
positionCommands = ["where am i", "where are we", "where are we located"]
startDrive = ["start driving", "keep driving", "start", "drive", "keep going"]
stopDrive = ["stop driving", "stop the car", "stop"]

nextBlock = ["next intersection", "next block", "block", "next intersection"]

#this is only for testing
testLocations = ["next intersection", "next block", "giant eagle",
                 "carnegie mellon university", "ali baba restaurant",
                 "eldridge street", "murray avenue", "heinz field", "new york",
                 "morewood"]
gotoCommands = ["goto", "go to"]


"""
This function checks if latlng is float formatted or not
"""


def isFloat(x):
    try:
        float(x)
        return True
    except:
        return False


class NavResponse:

    def PreprocessInput(self, input_text):
        #convert input_text to lowercase
        input_text = input_text.lower()
        if "go to" in input_text:
            input_text = input_text.replace("go to", "goto")

       #remove articles and other stops
        for word in ["the", "this", "then"]:
            if word in input_text:
                input_text = input_text.replace(word, "")
        return input_text

    def InControlCommands(self, input_text, user_location):
        command = ""
        prompt = ""
        for word in startDrive:
            if word in input_text:
                command = "cmd_mov:start"
                prompt = "okay, starting to drive"
                return "%s|%s" % (command, prompt)
            #return self.SendCommandToStreetView(input_text)

        for word in stopDrive:
            if word in input_text:
                command = "cmd_mov:stop"
                prompt = "okay, stopping"
                return "%s|%s" % (command, prompt)

        for word in speedMap:
            if word in input_text:
                command = "cmd_spd:%s" % speedMap[word]
                prompt = "let's go %s" % word
                return "%s|%s" % (command, prompt)

        for word in positionCommands:
            if word in input_text:
                return self.SendPositionResponse(input_text, user_location)

    """
    This function checks the OVERRIDE mode is activated
    if not it will append the incoming command to the queue
    """
    def InNavigationCommands(self, input_text, user_location):
        navcom = []
        navcom.extend(turnCommands)
        navcom.extend(gotoCommands)
        #print navcom
        response = ""
        for word in navcom:
            if word in input_text:
                self.commandQueue.append(input_text)
                if self.OVERRIDE or len(self.commandQueue) == 1:
                    input_text = self.commandQueue.pop(0)
                    response = self.SendNavigationCommands(input_text,
                                                           user_location)
                else:
                    response = "Queued to the list of executable commands"
                return response
    """
    startDrive
    stopDrive
    goto location #remember always use Google Places for POI
    goto street #remember always use GeoNames for Streets
    goto next block, next intersection
    turns: left or right
    speed: go fast, go slow, go normal, go faster
    positionCommands
    """
    def GetResponse(self, input_text, user_location, caller_id):
        """
        Arguments:
        - `self`:
        - `input_text`:
        - `user_location`:
        - `caller_id`:
        """
        print(input_text)

        input_text = self.PreprocessInput(input_text)

        response = self.InControlCommands(input_text, user_location)
        if response:
            return response
        """
        Interruptable commands
        check if the commandStatus is True otherwise return
        that the system is currently following a command
        see if the commands are overridable
        i.e. we can ignore the current command
        and follow the current command
        only if OVERRIDE = False we will append
        the commands in queue
        if OVERRIDE:
          follow the command
        else:
          commandQueue.append(command)
          if len(commandQueue) == 1:
            commandQueue.pop
            execute the command

        closeToDestination will execute subseq commands
        """

        response = self.InNavigationCommands(input_text, user_location)
        if response:
            return response

        response = self.SendSmallTalkResponse(input_text, caller_id)
        return response

    def SendCommandToStreetView(self, input_text):
        """
        check for commands
        cmd_mov, cmd_spd
        """
        command = ""
        prompt = ""
        if input_text in startDrive:
            command = "cmd_mov:start"
            prompt = "okay, starting to drive"
        elif input_text in stopDrive:
            command = "cmd_mov:stop"
            prompt = "okay, stopping the car"

        elif input_text.startswith("go") or input_text in speedMap:
            speed_text = input_text
            if ' ' in input_text:
                speed_text = input_text.split(" ", 1)[1]
                #this is just hacky if somebody says go very fast
            command = "cmd_spd:%s" % speedMap[speed_text]
            prompt = "let's go %s" % speed_text

        return "%s|%s" % (command, prompt)

    def SendNavigationCommands(self, input_text, user_location):

        """
        Here we change the lat/lng based on the input response
        latlng, bearing
        cmd_loc
        """
        dst_latlng = None

        prompt = ""
        """
        error handling code for user_location
        bearing could be undefined and the latitude,longtiude could be missing
        who knows
        """
        if not user_location:
            return "you can say, start driving."

        latlng, bearing = user_location
        lat, lng = latlng
        if isFloat(bearing) and isFloat(lat) and isFloat(lng):
            bearing = float(bearing)
            lat = float(lat)
            lng = float(lng)
            latlng = lat, lng
        else:
            return "looks like, the car was not started"
        """
        this is dumb thing to do because in real life roads
        are not always perpendicular to each other
        """

        if "right" in input_text:
            #brng = (bearing + 90) % 360
            #dst_latlng = self.navUtil.GetDestination(latlng, brng, 0.1)
            prompt = "let's take the next right"
            dst_latlng = self.GetDirectionBasedDest(latlng, bearing, 90)

        elif "left" in input_text:
            #brng = (bearing - 90) % 360
            #dst_latlng = self.navUtil.GetDestination(latlng, brng, 0.1)
            prompt = "let's take the next left"
            dst_latlng = self.GetDirectionBasedDest(latlng, bearing, -90)
        elif "around" in input_text:
            prompt = "turning around"
            brng = (bearing + 180) % 360
            dst_latlng = self.navUtil.GetDestination(latlng, brng, 0.01)
            #self.GetDirectionBasedDest(latlng, bearing, 180)
        elif "goto" in input_text:
            #dst_latlng = self.geoDBase.FindNearestIntersection(latlng,bearing)
            dst = "next block"
            dst_latlng = latlng
            prompt = ""
            dst = input_text.split("goto", 1)[1]
            dst = dst.strip()
            if "block" in dst or "intersection" in dst:
                    #print ("%s is next block\n" % dst)
                    dst_latlng = self.GetDirectionBasedDest(latlng, bearing, 0)
                    streetName = self.geoDBase.FindPlaceNearUs(dst_latlng)
                    prompt = "let's go to %s" % streetName

            elif "street" in dst or "avenue" in dst:
                locName = dst
                dst_latlng = self.geoDBase.FindStreetByName(latlng, locName)
                if dst_latlng:
                    prompt = "let's go to  %s" % locName
                else:
                    prompt = "sorry, can't find street %s", locName
            else:
                #print dst
                rad = 5000

                #see if the destination is a POI
                result = self.geoDBase.FindPOI(dst, latlng, rad)
                dst_latlng, locName, nearbyStreet = result
                if locName.startswith("ERROR"):
                    print ("%s not a POI\n" % dst)
                    dst_latlng = self.geoDBase.FindStreetByName(latlng, dst)
                    if dst_latlng:
                        prompt = "let's go to  %s" % dst
                    else:
                        print ("%s not a street\n" % dst)
                        dst_latlng, addr = self.geoDBase.FindOthers(dst)
                        if dst_latlng:
                            prompt = "let's go to %s near %s" % (dst, addr)
                        else:
                            print ("%s not a city or anything else\n" % dst)
                            return "sorry, can't find location %s" % dst
                else:
                    prompt = "let's go to %s near %s" % (dst, nearbyStreet)
                #prompt = "let's go to %s " % (dst)

        if dst_latlng:
            return "cmd_loc:%s|%s" % (','.join(map(str, dst_latlng)), prompt)
        else:
            return "sorry, can't find location %s" % dst

    def SendSmallTalkResponse(self, input_text, caller_id):
        return "Sorry, I did not get that"
        pass
        #return aiml.GetResponse(input_text,caller_id)

    def SendPositionResponse(self, input_text, user_location):
        """
        error handling code for user_location
        bearing could be undefined and the latitude,longtiude could be missing
        who knows
        """
        if not user_location:
            return "you can say, start driving."

        latlng, bearing = user_location
        lat, lng = latlng

        if isFloat(bearing) and isFloat(lat) and isFloat(lng):
            bearing = float(bearing)
            lat = float(lat)
            lng = float(lng)
            latlng = lat, lng
        else:
            return "sorry, cannot fetch your location."

        user_location_str = self.geoDBase.FindPlaceNearUs(latlng)
        return "We are on %s" % user_location_str

    def GetDirectionBasedDest(self, latlng, bearing, brngDiff):
        dst_latlng = latlng
        pts = self.geoDBase.FindNearbyIntersections(latlng, bearing)
        pts = self.geoDBase.FilterByBearing(latlng, bearing, pts)
        farthest_pt = pts[0]
        max_dist = 0
        if brngDiff == 0:
            for pt in pts:
                tmp_dist = self.navUtil.CalculateDistance(latlng,
                                                          pt,
                                                          "spherical")

                #check if the point is far enough to drive
                if tmp_dist >= 0.1:
                    farthest_pt = pts[0]
                    break

                #otherwise just go along with the sorting of points by dist
                if tmp_dist > max_dist:
                    max_dist = tmp_dist
                    farthest_pt = pt
            dst_latlng = self.geoDBase.FindNearestIntersection(farthest_pt)

        else:
            dst_latlng = self.geoDBase.FilterByFinalBearing(latlng,
                                                            bearing,
                                                            brngDiff,
                                                            pts)
        return dst_latlng

    """
    check if the command queue is empty
    then check if the commandStatus is True
    then pop the next command and call
    the next Command
    """
    def moveToNextCommand(self):
        input_text = self.commandQueue.pop(0)
        latlng = self.getCurrentPosition()
        yaw = self.getCurrentHeading()
        user_location = (latlng, yaw)
        self.cmdInAction = True
        self.currentDestination = ()
        self.currentPosition = ()
        self.OVERRIDE = True
        resp = self.SendNavigationCommands(input_text, user_location)
        self.callback_func(resp)

    def closeToDestination(self):
        currentPosition = self.getCurrentPosition()
        destination = self.getCurrentDestination()
        distance = self.navUtil.CalculateDistance(currentPosition,
                                                  destination,
                                                  "spherical")
        if distance < 0.1:
            return True
        else:
            return False

    def setCommandStatus(self, cmdInAction):
        self.cmdInAction = cmdInAction

    def getCommandStatus(self):
        return self.cmdInAction

    def setCurrentDestination(self, latlng):
        self.currentDestination = latlng

    def getCurrentDestination(self):
        return self.currentDestination

    def setCurrentPosition(self, latlng, yaw):
        self.currentPosition = latlng
        self.currentHeading = yaw

        if len(self.commandQueue) > 1 and self.closeToDestination():
            self.moveToNextCommand()

    def getCurrentPosition(self):
        return self.currentPosition

    def getCurrentHeading(self):
        return self.currentHeading

    def __init__(self, geoObj, navUtilObj, callback_func=None):
        """
        """
        self.geoDBase = geoObj
        self.navUtil = navUtilObj
        self.cmdInAction = False
        self.commandQueue = []
        self.currentDestination = ()
        self.currentPosition = ()
        self.OVERRIDE = True
        self.callback_func = callback_func


def test_callback_func(reply):
    print("%s" % reply)

if __name__ == "__main__":
    navUtilObj = NavUtil()
    geoObj = GeoNames(navUtilObj)

    source = None
    caller_id = "blah"
    navResObj = NavResponse(geoObj, navUtilObj, test_callback_func)

    TEST_NAV = True
    TEST_CONTROL = False
    TEST_POSITION = False
    TEST_OTHERS = False
    TEST_ALL = False

    if TEST_ALL or TEST_OTHERS:
        input_text = ""
        source = None
        caller_id = "blah"
        dest = navResObj.GetResponse(input_text, source, caller_id)
        print ("sending %s as response to %s\n" % (input_text, str(dest)))

    #latlng = (40.43045,-79.923435)
    #latlng = (40.444637, -79.943664)
    latlng = (40.431529, -79.923278)
    #(40.42983,-79.92255)
    bearing = 6.31
    source = (latlng, bearing)

    if TEST_ALL or TEST_NAV:
        gotoNavCommands = []
        for loc in testLocations:
            gotoNavCommands.append('lets go to ' + loc)
        turnCommands.extend(gotoNavCommands)
        for input_text in turnCommands:
            dest = navResObj.GetResponse(input_text, source, caller_id)
            print ("to %s from %s %s\n" % (input_text, str(latlng), str(dest)))

    if TEST_ALL or TEST_CONTROL:

        for sv_cmd in ["faster", "slow"]:
            sv_cmd = "let's go " + sv_cmd
            response = navResObj.GetResponse(sv_cmd, source, caller_id)
            print ("sending %s as response to %s\n" % (response, sv_cmd))

    if TEST_ALL or TEST_POSITION:
        for sv_cmd in positionCommands:
            response = navResObj.GetResponse(sv_cmd, source, caller_id)
            print ("sending %s as response to %s\n" % (response, sv_cmd))
