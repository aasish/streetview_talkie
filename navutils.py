#!/usr/bin/env python
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Aasish Pappu (aasish@cs.cmu.edu)                             ##
##  Date  : November 2011                                                ##
###########################################################################
## Description: Navigation Utilities to calculate Distance between two   ##
## points in space. It can also fetch destination latlng based on the    ##
## bearing and distance from source                                      ## 
###########################################################################                

from math import pi as PI
from math import sin, cos, tan, asin, acos, atan2, sqrt, fabs, log
RADIUS_OF_EARTH = 6371


class NavUtil:
    def Deg2Rad(self, deg):
        return deg * PI / 180

    def Rad2Deg(self, rad):
        return rad * 180 / PI

    #source and destination are tuples and returns distance in KiloMetres
    def CalculateDistance(self, source, destination, measure):
        lat1, lon1 = source
        lat2, lon2 = destination
        dLat = self.Deg2Rad(lat2 - lat1)
        dLon = self.Deg2Rad(lon2 - lon1)
        lat1 = self.Deg2Rad(lat1)
        lat2 = self.Deg2Rad(lat2)
        lon1 = self.Deg2Rad(lon1)
        lon2 = self.Deg2Rad(lon2)

        if measure == "haversine":
            a = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(lat1) * cos(lat2); 
            c = 2 * atan2(sqrt(a), sqrt(1-a));
            d = RADIUS_OF_EARTH * c
        if measure == "spherical":
            d = acos(sin(lat1)*sin(lat2) + cos(lat1)*cos(lat2) * cos(lon2-lon1)) * RADIUS_OF_EARTH;
        return d

    def GetBearing(self, source, destination):
        lat1, lon1 = source
        lat2, lon2 = destination

        dLon = self.Deg2Rad(lon2 - lon1)
        lat1 = self.Deg2Rad(lat1)
        lat2 = self.Deg2Rad(lat2)
        lon1 = self.Deg2Rad(lon1)
        lon2 = self.Deg2Rad(lon2)

        dPhi = log(tan(lat2/2 + PI/4) / tan(lat1/2 + PI/4))

        if fabs(dLon) > PI:
            if dLon > 0:
                dLon = -(2 * PI - dLon)
            else:
                dLon = (2 * PI + dLon)
        brng = atan2(dLon, dPhi)

        return (self.Rad2Deg(brng) + 360) % 360

    def GetDestination(self, source, bearing, distance):
        lat1, lon1 = source
        lat1 = self.Deg2Rad(lat1)
        lon1 = self.Deg2Rad(lon1)
        bearing = self.Deg2Rad(bearing)

        lat2 = asin(sin(lat1)*cos(distance/RADIUS_OF_EARTH) + cos(lat1)*sin(distance/RADIUS_OF_EARTH)*cos(bearing))
        lon2 = lon1 + atan2(sin(bearing)*sin(distance/RADIUS_OF_EARTH)*cos(lat1), cos(distance/RADIUS_OF_EARTH)-sin(lat1)*sin(lat2))

        return (self.Rad2Deg(lat2), self.Rad2Deg(lon2))

    def __init__(self):
        pass

if __name__ == "__main__":

    navUtilObj = NavUtil()
    source = (50.0359, -005.4253)
    destination = (58.3838, -003.0412)

    print("%d km is the haversine distance \n" %navUtilObj.CalculateDistance(source,destination,'haversine'))
    print("%d km is the spherical distance \n" %navUtilObj.CalculateDistance(source,destination,'spherical'))
    source = (53.1914, -001.4347)
    distance = 1
    bearing = 096.0118
    print("%s is the source distance \n" % str(navUtilObj.GetDestination(source,bearing,distance)))
