#!/usr/bin/env python
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Aasish Pappu (aasish@cs.cmu.edu)                             ##
##  Date  : November 2011                                                ##
###########################################################################
## Description: This is a freeswitch app for Navigation Dialog System    ##
## with StreetView as interface                                          ##
###########################################################################

import os
from freeswitch import *
import tempfile
import subprocess
import shutil
debug = True
if debug:
    import navdialog
    reload(navdialog)
    import navdb
    reload(navdb)
    import navutils
    reload(navutils)

from navdialog import NavResponse
from navdb import GeoNames
from navutils import NavUtil


#from aiml_response import AIMLResponse

endpointbin = "/usr4/apappu/projects/AudioServer/audio_client"
google_port = "9993"
psphinx_port = "9995"
ap = None
sess = None
uid = None
# HANGUP HOOK
#
# session is a session object
# what is "hangup" or "transfer"
# WARNING: known bugs with hangup hooks, use with extreme caution


def send_response_to_streetview(nav_reply):
    global ap
    global sess
    global uid
    ttsreply = ""
    nav_cmd = ""
    if nav_reply:
        if nav_reply.startswith("cmd_"):
            nav_cmd, ttsreply = nav_reply.split("|")
                    #otherwise just send the reply as it is
        else:
            ttsreply = nav_reply

    ap.executeString("uuid_chat %s Chauffeur: %s" % (uid, ttsreply))
    sess.speak(ttsreply)
    if nav_cmd:
        ap.executeString("uuid_chat %s %s" % (uid, nav_cmd))


def hangup_hook(session, what):
    consoleLog("info", "hangup hook for %s!!\n\n" % what)
    return


def input_callback(session, what, obj):

    if (what == "dtmf"):
        consoleLog("info", what + " " + obj.digit + "\n")
    else:
        consoleLog("info", what + " " + obj.getBody() + "\n")
        #user_location = obj.getBody()
        #return obj.getBody()
    return

# APPLICATION
#
# session is a session object
# args is all the args passed after the module name


def handler(session, args):
    global ap
    global sess
    global uid

    navigateUtilObj = NavUtil()
    geoDBase = GeoNames(navigateUtilObj)

    nav = NavResponse(geoDBase, navigateUtilObj, send_response_to_streetview)

    api = API()
    user_location = ""
    session.answer()
    sess = session
    con = EventConsumer("CUSTOM", "rtmp::clientcustom")
    decoderport = google_port
    #isPhone = False

    uuid = session.getVariable("uuid")
    uid = uuid
    ap = api
    user_name = session.getVariable("caller_id_name")
    user_num = session.getVariable("caller_id_number")
    consoleLog("info", "Reached by uuid: %s\n" % uuid)
    #api.executeString("uuid_chat %s uid:%s"%(uuid, uuid))
    consoleLog("info", "Reached by user_name: %s\n" % user_name)
    consoleLog("info", "Reached by user_num: %s\n" % user_num)
    session.set_tts_parms("flite", "awb")

    if len(user_num) == 10 and user_num.isdigit():
        consoleLog("info", "%s is a phone number\n" % user_name)
        #isPhone = True

    wl_msg = "This call may be recorded\
    anonymously and used for research purposes."
    api.executeString("uuid_chat %s %s" % (uuid, wl_msg))
    session.speak(wl_msg)

    def sendMessageToStreetView(uid, message):
        api.executeString("uuid_chat %s %s" % (uid, message))

    sendMessageToStreetView(uuid, "cmd_uid:%s" % uuid)
    #session.setHangupHook(hangup_hook)
    # if(isPhone):
    #     session.set_tts_parms("flite","rms")
    #      msg = """Please listen carefully.
    #           To talk with, system 1, press 1.
    #           To talk with, system 2, press 2.  """
    #     session.speak()
    #     digit = session.getDigits(1,'#',10000)
    #     consoleLog("info", "Got dtmf: %s\n"%digit);
    #     if digit == "2":
    #         consoleLog("info", "using sphinx decoder, good luck\n");
    #         decoderport = psphinx_port
    #welcome_msg = "Please hold on for just a few seconds..."
    #session.speak(welcome_msg)

    session.set_tts_parms("flite", "slt")
    welcome_msg = "Hello, I am a Virtual Chauffeur!\
                  You can ask me to drive, stop driving."
    api.executeString("uuid_chat %s %s" % (uuid, welcome_msg))
    session.speak(welcome_msg)
    session.setVariable("RECORD_READ_ONLY", "true")
    session.setVariable("record_sample_rate", "16000")

    welcome_msg = "And if you are in hurry\n\
                  you can say go fast, go very fast or go slow.\
                  Where do you want to go today?"
    #api.executeString("uuid_chat %s %s" % (uuid, welcome_msg))
    #session.speak(welcome_msg)
    # Create a temporary directory for pipes and files
    tmpdir = tempfile.mkdtemp(prefix="teamtalk-")
    session.setVariable("teamtalk-temp-directory", tmpdir)
    consoleLog("info", "tmpdirectory is %s " % tmpdir.split("/")[2])
    tmpdirname = tmpdir.split("/")[2]

    wavpipe = os.path.join(tmpdir, 'wav.pipe.r16')
    txtpipe = os.path.join(tmpdir, 'recognized.text.pipe')

    os.mkfifo(wavpipe)
    os.mkfifo(txtpipe)

    p = subprocess.Popen([endpointbin,
                          wavpipe,
                          txtpipe,
                          decoderport,
                          tmpdirname])

    reply = api.executeString("uuid_record %s start %s" % (uuid, wavpipe))
    consoleLog("info", "Received uuid_record reply: %s\n" % reply)

    asrout = os.open(txtpipe, os.O_RDONLY | os.O_NONBLOCK)

    buffer = ""
    play_progressbar = False
    play_tts = False

    #current_session_id = ""

    while True:
        if session.getState() != "CS_EXECUTE":
            break

        asrhyp = ""
        ttsreply = ""
        nav_cmd = ""
        hyptype = ""
        hypvalue = ""
        chat_msg = ""
        #consoleLog("info","before popping message for latlng\n")
        location_msg = con.pop()
        #consoleLog("info","after popping message for latlng\n")
        if location_msg:
            #consoleLog("info","before extracting uid\n")
            msg_session_id = location_msg.getHeader("uid")
            #consoleLog("info","after popping uid\n")
            if msg_session_id == uuid:
                consoleLog("info", "reached by %s\n" % msg_session_id)
                chat_msg = location_msg.getHeader("chat")
                if chat_msg:
                    asrhyp = 'utthyp:%s' % chat_msg
                    consoleLog("info", "msg is %s\n" % asrhyp)
                else:
                    yaw = location_msg.getHeader("yaw")
                    lat = location_msg.getHeader("lat")
                    lng = location_msg.getHeader("lng")
                    #consoleLog("info", "y:%s,t:%s,lg:%s\n" % (yaw, lat, lng))
                    user_location = ((lat, lng), yaw)
                    nav.setCurrentPosition((lat, lng), yaw)

        try:
            more = os.read(asrout, 255)
            buffer += more
        except OSError:
            pass

        lines = buffer.split('\n', 1)
        if len(lines) > 1:
            asrhyp = lines[0].strip()
            buffer = lines[1]

        if asrhyp:
            hyptype, hypvalue = asrhyp.split(':', 1)
            hypvalue = hypvalue.lower()
            consoleLog("info", "%s to %s from asrout\n" % (hyptype, hypvalue))

            if hyptype == "endutt":
                play_progressbar = True
                play_tts = False
            elif hyptype == "startutt":
                play_tts = False
                play_progressbar = False
            elif hyptype == "utthyp":
                play_progressbar = False
                if hypvalue:
                    play_tts = True

                    nav_reply = nav.GetResponse(hypvalue, user_location, uuid)
                    consoleLog("info", "%s from navdb\n" % nav_reply)

                    if nav_reply:
                        if nav_reply.startswith("cmd_"):
                            nav_cmd, ttsreply = nav_reply.split("|")
                    #otherwise just send the reply as it is
                        else:
                            ttsreply = nav_reply
                    else:
                        ttsreply = "cannot understand your command"

                else:
                    play_tts = False
                    ttsreply = "Please, speak up a little"

        if play_progressbar:
            pass
            #session.streamFile("/usr4/apappu/tmp/filler.wav")
        elif play_tts:
            api.executeString("uuid_chat %s I heard you say: %s"
                              % (uuid, hypvalue))
            api.executeString("uuid_chat %s Chauffeur: %s" % (uuid, ttsreply))
            reply = api.executeString("uuid_audio %s start read mute -4" %(uuid))
            consoleLog("info", "Start non-listening uuid_audio reply: %s\n" %reply)
            session.sleep(100)
            session.speak(ttsreply)
            session.sleep(300)
            reply = api.executeString("uuid_audio %s stop" %(uuid))
            consoleLog("info", "Stop non-listening uuid_audio reply: %s\n" %reply)

            if nav_cmd:
                api.executeString("uuid_chat %s %s" % (uuid, nav_cmd))

            play_tts = False

        else:
            session.sleep(300)

    reply = api.executeString("uuid_record %s stop %s" % (uuid, wavpipe))

    consoleLog("info", "Received uuid_record stopping reply: %s\n" % reply)
    session.unsetInputCallback()
    shutil.rmtree(tmpdir)
    p.terminate()
    session.hangup()
