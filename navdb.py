#!/usr/bin/env python
###########################################################################
##                                                                       ##
##                  Language Technologies Institute                      ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2011                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Author: Aasish Pappu (aasish@cs.cmu.edu)                             ##
##  Date  : November 2011                                                ##
###########################################################################
## Description: This script talks with GeoNames and GOOG database
###########################################################################

import simplejson
import urllib
import pickle
from navutils import NavUtil
from math import fabs

##########################
#BEGIN OF Database Variables
#########################
GEO_APP_ID = 'teamtalk'
GEO_SEARCH_BASE = 'http://api.geonames.org/'
#for google we are going to use the following
#GOOG_APP_ID = 'AIzaSyD9SLCyByO96YyNEvSmNVxIEXx9cWQfUvk'
GOOG_APP_ID = 'AIzaSyD9SLCyByO96YyNEvSmNVxIEXx9cWQfUvk'
#GOOG_APP_ID = 'AIzaSyCVbEe9_3mH4XnktP07HsTh3i4mR1mJLVo'
#GOOG_PLACES = 'https://maps.googleapis.com/maps/api/place/search/json'
GOOG_PLACES = 'https://maps.googleapis.com/maps/api/place/textsearch/json'
GOOG_GEOCODE = 'https://maps.googleapis.com/maps/api/geocode/json'
GOOG_DIRECTIONS = 'https://maps.googleapis.com/maps/api/directions/json'
##########################
#END OF Database Variables
#########################

##########################
#BEGIN OF Other Variables
#########################
BEARING_THRESHOLD = 90  # default tolerance value of change in bearing
RADIUS_OF_VICINITY = 5000  # default radius value for google places
##########################
#END OF Database Variables
#########################


class GeoNamesError(Exception):
    pass


class GeoNames:
    """
    takes searchlocation (type:name), latlng (current location)
    print("%s,%s,%s\n" %
    (res['geometry']['location'],
    res['vicinity'],
    res['name']))
    returns     dest_latlng,humanReadableName of the Street
    """
    def FindOthers(self, searchlocation, **kwargs):

        kwargs.update({
                'address': searchlocation,
                'sensor': 'false',
                })
        url = GOOG_GEOCODE + '?' + urllib.urlencode(kwargs)
        #print ("resultant url is %s\n" % url)
        result = simplejson.load(urllib.urlopen(url))
        if '' in result:
            raise GeoNamesError
        results = result['results']
        if len(results) < 1:
            return None, "ERROR:Can't find %s" % searchlocation
        res = results[0]
        #sort locations based on the current location
        dest_latlng = res['geometry']['location']
        lat, lng = dest_latlng['lat'], dest_latlng['lng']
        dest_latlng = (lat, lng)
        humanReadableName = res['formatted_address']

        return dest_latlng, str(humanReadableName)
    """
    takes searchlocation (type:name), latlng (current location)
    print("%s,%s,%s\n" %
    (res['geometry']['location'],
    res['vicinity'],
    res['name']))
    returns     dest_latlng,humanReadableName,nearbyStreet
    """
    def FindPOI(self, searchlocation, latlng, radius, **kwargs):
        lat, lng = latlng
        kwargs.update({
                'key': GOOG_APP_ID,
                'location': ','.join(map(str, [lat, lng])),
                'sensor': 'false',
                'radius': radius,
                'query': searchlocation
                })
        url = GOOG_PLACES + '?' + urllib.urlencode(kwargs)
        print ("resultant url is %s\n" % url)
        result = simplejson.load(urllib.urlopen(url))
        if '' in result:
            raise GeoNamesError
        results = result['results']
        #print results
        if len(results) < 1:
            return None, "ERROR:Can't find %s" % searchlocation, ""
        res = results[0]
        #sort locations based on the current location
        dest_latlng = res['geometry']['location']
        lat, lng = dest_latlng['lat'], dest_latlng['lng']
        dest_latlng = lat, lng
        humanReadableName = res['name']
        if 'formatted_address' in res:
            nearbyStreet = res['formatted_address']
        elif 'vicinity' in res:
            nearbyStreet = res['vicinity']

        return dest_latlng, str(humanReadableName), str(nearbyStreet)
    """
    takes the source and destination latlng
    fetches directions using GOOG_DIRECTIONS API
    this is a filter to check if the bearing changes drastically.
    assumes that we are using ONE leg although API supports multiple legs
    """
    def GetDirections(self, origin, destination, **kwargs):
        kwargs.update({
                'origin': origin,
                'destination': destination,
                'sensor': 'false',
                })
        url = GOOG_DIRECTIONS + '?' + urllib.urlencode(kwargs)
        #print ("resultant url is %s\n" % url)
        result = simplejson.load(urllib.urlopen(url))
        #print result['status'], dir(result['routes'][0]['legs'][0]['steps'][0])
        if result['status'] == 'OK':
            results = result['routes'][0]['legs'][0]['steps'][0]
            #print result['routes'][0]['legs'][0]['steps'][0]
        else:
            raise GeoNamesError
        if len(results) < 1:
            return "ERROR:Can't find directions"
        return results

    """
    what's the bearing to an exit point from the current point
    to filter out intersections that are behind us, unless specified explicitly
    """
    def FilterByBearing(self, latlng, bearing, exit_points_on_street):
        #first we will check the bearingDiff
        exit_points = exit_points_on_street
        #discard if too far from the bearing threshold
        for point in exit_points:
            final_brng = self.navUtilObj.GetBearing(latlng, point)
            currentBearingDiff = bearing - final_brng
            if fabs(currentBearingDiff) > BEARING_THRESHOLD:
                #print "removing a point %s" %self.FindPlaceNearUs(point)
                exit_points_on_street.remove(point)
            
               

        #update the exit points
        return exit_points_on_street

    """
    discard if the suggested directions are off the bearing threshold.
    This function is handy when we want to turn right/left at the
    upcoming intersection. However we are not sure if there are tiny
    intersections that have inaccessible turns. So this function
    filters the intersections to unreachable roads (eg. private roads)
    To do that we need to get directions first and check
    if the bearing changes drastically
    eg. if a private road is accessible from another place
    which requires the car to turn around! Then avoid it.
    """
    def FilterByFinalBearing(self, latlng, bearing, brng_change, exit_points):
        #first we fetch directions between exit_point and final_destlatlng
        #brng change wuld be +90 otherwise -90
        for point in exit_points:
            dest = point
            closestAddr = self.FindNearestIntersection(dest)
            dest_bearing = self.navUtilObj.GetBearing(latlng, closestAddr)
            closestPlace = self.FindPlaceNearUs(closestAddr)
            #print "bearing diff is %s" % (bearing - dest_bearing)
            #print (" %s of %s \n" % (closestAddr, closestPlace))
            brng = (dest_bearing + brng_change) % 360
            dst_latlng = self.navUtilObj.GetDestination(closestAddr, brng, 0.1)
            #get directions between latlng and dst_latlng
            #then check if the FIRST step changes the bearing drastically
            directionsObj = self.GetDirections(latlng, dst_latlng)
            #check for ERROR
            if str(directionsObj).startswith("ERROR"):
                continue
            else:
                #unpack the directionsObj
                first_step = directionsObj
                way_pt = first_step['end_location']
                
                print way_pt, first_step['start_location'], first_step['html_instructions']
                lat, lng = way_pt['lat'], way_pt['lng']
                waypt_latlng = (float(lat), float(lng))
                waypt_brng = self.navUtilObj.GetBearing(latlng, waypt_latlng)
                print bearing, waypt_brng
                if fabs(bearing - waypt_brng) > BEARING_THRESHOLD:
                    continue
                else:
                    return dst_latlng

    """
    get the closest exit_point_on_street
    """
    def FilterByDistance(self, latlng, exit_points_on_street):
        pass

    """
    fetches end points of a street
    a street can have multiple entry points
    first fetch all entry points from the current point on the street
    """
    def FindNearbyIntersections(self, latlng, bearing):
        result = self.FetchJSON('findNearbyStreets', latlng)
        currentStreet = self.FindPlaceNearUs(latlng)
        streetList = result['streetSegment']
        exit_points_on_street = []
        for street in streetList:
            #there can be multiple entries for the same street
            if currentStreet == street['name']:
                exit_points = street['line'].split(',')
                #just take the endpoints not the intermediate
                for loc in [exit_points[0], exit_points[-1]]:
                    tmp_latlng = loc.split(' ')
                    #its alway longitude first in GeoNames
                    lat, lng = float(tmp_latlng[1]), float(tmp_latlng[0])
                    #see if the this point will change the heading
                    tmp_latlng = (lat, lng)
                    exit_points_on_street.append(tmp_latlng)
        return exit_points_on_street

    def FindNearbyStreets(self, latlng, bearing):
        result = self.FetchJSON('findNearbyStreets', latlng)
        currentStreet = self.FindPlaceNearUs(latlng)
        streetList = result['streetSegment']
        lat = None
        lng = None
        bearingDiff = ""
        prevLatLng = latlng
        for street in streetList:
            if currentStreet == street['name']:
                continue
            lng, lat = street['line'].split(',')[1].split(' ')
            dest_latlng = float(lat), float(lng)
            final_bearing = self.navUtilObj.GetBearing(latlng, dest_latlng)
            currentBearingDiff = bearing - final_bearing

            print ("%s\n" % currentBearingDiff)
            if bearingDiff:
                if currentBearingDiff > bearingDiff:
                    return prevLatLng
                else:
                    bearingDiff = currentBearingDiff
                    prevLatLng = dest_latlng
            else:
                bearingDiff = currentBearingDiff
                prevLatLng = dest_latlng

        return (lat, lng)

    def FindStreetByName(self, latlng, streetName):
        result = self.FetchJSON('findNearbyStreets', latlng)
        streetList = result['streetSegment']
        lat = None
        lng = None
        for street in streetList:
            lng, lat = street['line'].split(',')[1].split(' ')
            dest_latlng = float(lat), float(lng)
            if ' ' in streetName:
                streetNameTokens = streetName.split(' ')
                for token in streetNameTokens:
                    if token and token in street['name'].lower():
                        return dest_latlng
        return None

    def Geolocate(self, latlng):
        pass

    """
    This method takes latlng and returns the address object
    it has the following
    street
    placename
    postalcode
    distance  -- from current latlng
    lat,lng
    """
    def FindPlaceNearUs(self, latlng):
        result = self.FetchJSON('findNearestAddress', latlng)
        return (result['address']['street'])

    def FindNearestAddress(self, latlng):
        result = self.FetchJSON('findNearestAddress', latlng)
        return (result['address']['lat'], result['address']['lng'])

    """
     returns nearest intersection
     useful dict entries are
     street1
     street2
     distance
     placename
     lat,lng
    """
    def FindNearestIntersection(self, latlng):
        result = self.FetchJSON('findNearestIntersection', latlng)
        """
        Not necessarily closest intersection is the intended intersection
        """
        lat = float(result['intersection']['lat'])
        lng = float(result['intersection']['lng'])
        return (lat, lng)

    def FetchJSON(self, queryType, latlng, **kwargs):
        lat, lng = latlng
        kwargs.update({
                'username': GEO_APP_ID,
                'lat': lat,
                'lng': lng,
                'formatted': 'true',
                'style': 'full'
                })
        queryType = queryType + 'JSON'
        url = GEO_SEARCH_BASE + queryType + '?' + urllib.urlencode(kwargs)
        #print ("resultant url is %s\n" % url)
        result = simplejson.load(urllib.urlopen(url))
        if '' in result:
            raise GeoNamesError
        return result

    def FindNearbyStreetsCached(self, streetList, N):
        """
        #example latlngSequence
        '-122.179753 37.451953,-122.17997 37.451692'
        Arguments:
        - `self`:
        - `streetList`: list of streetNames
        - `N`: get the Nth closest street
        """
        if N > len(streetList):
            raise "N is bigger than number of streets\n"
        for street in streetList:
            print ("%s is %s away\n" % (street['name'], street['distance']))
            #latlngList = latlngSequence.split(',')
        lng, lat = streetList[N]['line'].split(',')[1].split(' ')
        return (lat, lng)

    def __init__(self, navutil):
        self.navUtilObj = navutil

if __name__ == "__main__":

    navutilObj = NavUtil()
    navobj = GeoNames(navutilObj)

    """
    lat, lng = 37.451, -122.18
    location_query = "findNearbyStreets"
    data = navobj.FetchJSON(location_query,lat,lng)
    out = open('data.pk1','wb')
    pickle.dump(data,out)
    out.close()
    """
    inf = open('data.pk1', 'rb')
    data = pickle.load(inf)
    #get Nth closest Street
    N = 2
    """
    closestAddress = navobj.FindNearbyStreetsCached(data['streetSegment'],N);
    print ("entry point to closest street is %s \n" %(closestAddress))
    latlng = (40.42983, -79.92255)
    bearing = 270

    # use this to find the nearest-"logical"-intersection
    closestAddress = navobj.FindNearbyStreets(latlng, bearing)
    closestPlace = navobj.FindPlaceNearUs(closestAddress)
    print ("closest street is %s of %s \n" % (closestAddress, closestPlace))
    """
    latlng = (40.445098, -79.942742)
    
    bearing = 23
    #(40.444899, -79.942854)
    #latlng = (40.445993, -79.942268)
    #bearing = 23
    # line1 = "-79.942257 40.446008,-79.942378 40.44573,-79.942508 40.445506,-79.942814 40.444981,-79.943002 40.444647"
    # line2 = "-79.942365 40.446823,-79.942257 40.446008"
    # lines = [line1, line2]
    # for line in lines:
    #     latlngs = line.split(",")
    #     for loc in [latlngs[0], latlngs[-1]]:
    #         lng, lat = loc.split(' ')
    #         dest = (float(lat), float(lng))
    #         dest_bearing = navutilObj.GetBearing(latlng, dest)
    #         #print "bearing diff is %s" % (bearing - dest_bearing)
    #     #closestAddress = navobj.FindNearbyStreets(dest, dest_bearing)
    #         closestAddress = navobj.FindNearestIntersection(dest)
    #         dest_bearing = navutilObj.GetBearing(latlng, closestAddress)
    #         closestPlace = navobj.FindPlaceNearUs(closestAddress)
    #         print "bearing diff is %s" % (bearing - dest_bearing)
    #         print ("closest street is %s of %s \n" % (closestAddress, closestPlace))
    #         brng = (dest_bearing + 90) % 360
    #         prompt = "let's take the next right"
    #         dst_latlng = navutilObj.GetDestination(closestAddress, brng, 0.1)
    #         print "%s to %s" % (prompt, dst_latlng)

    #         brng = (dest_bearing - 90) % 360
    #         prompt = "let's take the next left"
    #         dst_latlng = navutilObj.GetDestination(closestAddress, brng, 0.1)
    #         print "%s to %s" % (prompt, dst_latlng)
 
    
    pts = navobj.FindNearbyIntersections(latlng, bearing)
    print pts
    pts = navobj.FilterByBearing(latlng, bearing, pts)
    print pts
    # for pt in pts:
    #     print("let's go to %s" % navobj.FindPlaceNearUs(pt))

    #dest_latlng = navobj.FilterByFinalBearing(latlng, bearing, 90, pts)
    #print ("we will turn right to %s %s" % (dest_latlng, navobj.FindPlaceNearUs(dest_latlng) ))
    #dest_latlng = navobj.FilterByFinalBearing(latlng, bearing, -90, pts)
    #print ("we will turn left to %s %s" % (dest_latlng, navobj.FindPlaceNearUs(dest_latlng)))
    #dest_latlng = navobj.FilterByFinalBearing(latlng, bearing, 0, pts)
    for pt in pts:
        print pt
        dest_latlng = navobj.FindNearestIntersection(pt)
        print ("we will go to %s %s", (pt, navutilObj.CalculateDistance(latlng, pt, "spherical")))
        print ("we will go to %s %s" % (dest_latlng, navobj.FindPlaceNearUs(dest_latlng)))
    """
    #print location vicinity and name of the POI
    searchlocation = "giant eagle"
    radius = RADIUS_OF_VICINITY
    result = navobj.FindPOI(searchlocation, latlng, radius)
    dest_latlng, name, street = result

    print("%s,%s,%s\n" % (dest_latlng, name, street))
    """
    inf.close()
